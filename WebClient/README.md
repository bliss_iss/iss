#Angular App Structure

##src/app/assets
Aici vin constantele aplicatiei, stuff like that (de exemplu endpoint-uri pentru api)
	
##src/app/components
Componente generale, se subintelege din componentele care sunt deja acolo
	
##src/app/modules
Aici vor intra toate modulele din aplicatie. Daca vrem o pagina noua, se va face un nou folder in modules cu numele paginii (**for the love of god, use lowercare for everything**) si se va urma structura de la modulul de home (*module.ts* si  *service.ts* se pot genera cu **ng generate [module/service] [nume]**, si *routing.ts* se creeaza manual pentru ca nu mai  stiu cum doamne se genereaza cu ng), apoi in folderul de components se vor genera toate sub-componentele din acel modul cu **ng generate component [nume]** (si apoi puteti sterge fisierul *.spec.ts*, it's for testing purposes, not needed).

##src/app/models
Clase din domain, aici ar trebui sa intre toate
	
##src/app/services
Servicii pentru conexiuni cu api-ul, stuff like that. Teoretic ar trebui sa se faca un service pentru fiecare tip de comunicare cu api-ul. Aka un account service care se foloseste pentru login, un organizer service pentru tot ce tine de un organizator, etc.

##src/app/app-routing.module.ts
Aici se definesc toate rutele din aplicatie. Am creat deja ruta pentru home si orice alta pagina care nu exista in client te va duce pe not-found. urmariti structura pentru aia de home si ar trebui sa fie totul ok.

##src/app/app-module.ts
In fisierul asta se vor incarca toate modulele care necesita eager loading (aka cele generale din src/app/components), in caz ca se vor mai adauga (un bad request page pentru call-urile la server eronate de exemplu).


####In rest, n-ar mai trebui sa fie chestii complicate de adaugat.

**O sa folosim Bootstrap sigur**, ala se va adauga in *angular.json* parca, dar sunt tutoriale pe net pentru asta. One more
thing, poate ne va usura mult munca ceva de pe **PrimeNG**, dati o geana si vedeti daca am avea nevoie in aplicatia noastra
de ceva de pe acolo.

Daca nu intelegeti ceva, intrebati.