import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { HomeModuleRouting } from './home.routing';
import { RouterModule } from '@angular/router';
import { HomeService } from './home.service';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    RouterModule,
    HomeModuleRouting
  ],
  providers: [
    HomeService
  ]
})
export class HomeModule { }
