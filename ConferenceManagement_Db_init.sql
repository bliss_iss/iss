--CREATE DATABASE ConferenceManagement
USE ConferenceManagement
GO

IF OBJECT_ID('Ticket', 'U') IS NOT NULL
	DROP TABLE Ticket
IF OBJECT_ID('Listener', 'U') IS NOT NULL
	DROP TABLE [Listener]
IF OBJECT_ID('Session', 'U') IS NOT NULL
	DROP TABLE [Session]
IF OBJECT_ID('Room', 'U') IS NOT NULL
	DROP TABLE Room
IF OBJECT_ID('ConferenceCommittee', 'U') IS NOT NULL
	DROP TABLE ConferenceCommittee
IF OBJECT_ID('Conference', 'U') IS NOT NULL
	DROP TABLE Conference
IF OBJECT_ID('AuthorDeadlines', 'U') IS NOT NULL
	DROP TABLE AuthorDeadlines
IF OBJECT_ID('EvaluationDeadlines', 'U') IS NOT NULL
	DROP TABLE EvaluationDeadlines
IF OBJECT_ID('Proposal', 'U') IS NOT NULL
	DROP TABLE Proposal
IF OBJECT_ID('Bid', 'U') IS NOT NULL
	DROP TABLE Bid
IF OBJECT_ID('Evaluation', 'U') IS NOT NULL
	DROP TABLE Evaluation
IF OBJECT_ID('Contribution', 'U') IS NOT NULL
	DROP TABLE Contribution
IF OBJECT_ID('Paper', 'U') IS NOT NULL
	DROP TABLE Paper
IF OBJECT_ID('Abstract', 'U') IS NOT NULL
	DROP TABLE Abstract
IF OBJECT_ID('Author', 'U') IS NOT NULL
	DROP TABLE Author
IF OBJECT_ID('Reviewer', 'U') IS NOT NULL
	DROP TABLE Reviewer
IF OBJECT_ID('PCMember', 'U') IS NOT NULL
	DROP TABLE PCMember
GO

CREATE TABLE PCMember(
	PCId INT PRIMARY KEY IDENTITY(1, 1),
	Username VARCHAR(30),
	[Password] VARCHAR(30),
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	Email VARCHAR(100),
	Affiliation VARCHAR(50),
	WebPage VARCHAR(50)
)

CREATE TABLE Reviewer(
	PCId INT FOREIGN KEY REFERENCES PCMember(PCId) UNIQUE
	--Username VARCHAR(30),
	--[Password] VARCHAR(30),
)

CREATE TABLE Author(
	AuthorId INT PRIMARY KEY IDENTITY(1, 1),
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	Email VARCHAR(100),
	Affiliation VARCHAR(50),
	[Password] VARCHAR(30)
)

CREATE TABLE Abstract(
	AbstractId INT PRIMARY KEY IDENTITY(1, 1),
	AbstractName VARCHAR(50),
	[Description] VARCHAR(200),
	Keywords VARCHAR(200)
)

CREATE TABLE Paper(
	PaperId INT PRIMARY KEY IDENTITY(1, 1),
	Title VARCHAR(50),  
	[Status] VARCHAR(50) DEFAULT 'NEW',
	PaperUrl TEXT,
	AbstractId INT FOREIGN KEY REFERENCES Abstract(AbstractId) UNIQUE
)

CREATE TABLE Contribution(
	AuthorId INT FOREIGN KEY REFERENCES Author(AuthorId),
	AbstractId INT FOREIGN KEY REFERENCES Abstract(AbstractId),
	PRIMARY KEY(AuthorId, AbstractId)
)

CREATE TABLE Evaluation(
	PaperId INT FOREIGN KEY REFERENCES Paper(PaperId),
	ReviewerId INT FOREIGN KEY REFERENCES Reviewer(PCId),
	PRIMARY KEY(PaperId, ReviewerId),
	Mark VARCHAR(30) DEFAULT 'NOT PROCESSED',
	Recommendation TEXT DEFAULT 'EMPTY'
)

CREATE TABLE Bid(
	AbstractId INT FOREIGN KEY REFERENCES Abstract(AbstractId),
	PCId INT FOREIGN KEY REFERENCES PCMember(PCId),
	PRIMARY KEY(AbstractId, PCId),
	[Status] VARCHAR(30)
)

CREATE TABLE Proposal(
	AbstactId INT FOREIGN KEY REFERENCES Abstract(AbstractId) UNIQUE,
	MetaInfo VARCHAR(200)
)

CREATE TABLE Conference(
	ConfId INT PRIMARY KEY IDENTITY(1, 1),
	ChairId INT FOREIGN KEY REFERENCES PCMember(PCId),
	[Name] VARCHAR(100),
	[Date] DATE,
	SecretKey VARCHAR(666) DEFAULT 'unicorns are real'
)

CREATE TABLE AuthorDeadlines (
	DId INT PRIMARY KEY IDENTITY(1, 1),
	ConfId INT FOREIGN KEY REFERENCES Conference(ConfId) UNIQUE,
	AbstractDeadline DATE,
	PaperDeadline DATE
)

CREATE TABLE EvaluationDeadlines (
	DId INT PRIMARY KEY IDENTITY(1, 1),
	ConfId INT FOREIGN KEY REFERENCES Conference(ConfId) UNIQUE,
	BiddingDeadline DATE,
	ReviewingDeadline DATE,
)

/* nu stim ce e cu asta */
CREATE TABLE ConferenceCommittee(
	ConfId INT FOREIGN KEY REFERENCES Conference(ConfId),
	PCId INT FOREIGN KEY REFERENCES PCMember(PCId),
	PRIMARY KEY (ConfId, PCId),
	--[Role] VARCHAR(30)
)

CREATE TABLE Room(
	RoomId INT PRIMARY KEY IDENTITY(1, 1),
	[Name] VARCHAR(50), 
	Capacity INT DEFAULT 100
)

INSERT INTO Room ([Name]) VALUES ('1'),('2'),('11'),('12'),('21') 

CREATE TABLE [Session](
	SessionId INT PRIMARY KEY IDENTITY(1, 1),
	SessionName VARCHAR(30),
	ConferenceId INT FOREIGN KEY REFERENCES Conference(ConfId),
	SessionChairId INT FOREIGN KEY REFERENCES PCMember(PCId),
	SpeakerId INT FOREIGN KEY REFERENCES Author(AuthorId),
	RoomId INT FOREIGN KEY REFERENCES Room(RoomId),
	[Time] VARCHAR(10) 
)

CREATE TABLE [Listener](
	ListenerId INT PRIMARY KEY IDENTITY(1, 1),
	Email VARCHAR(30),
	[Password] VARCHAR(30) 
)

CREATE TABLE Ticket(
	SessionId INT FOREIGN KEY REFERENCES [Session](SessionId),
	ListenerId INT FOREIGN KEY REFERENCES [Listener](ListenerId),
	Price INT DEFAULT 50
	PRIMARY KEY(SessionId,ListenerId)
)

INSERT INTO PCMember (Username,[Password],FirstName,LastName,Email,Affiliation,WebPage) VALUES
	('johnny','bambi','John','Doe','john.doe@yahoo.com','researcher','www.john_doe.com'),
	('alice','wonderland','Alice','Wonderland','alice.wonder@yahoo.com','wonderer','www.in_wonderland.com'),
	('bobby','alice','Bobby','Brown','bobby.brown@gmail.com','hopeful romantic','www.alice_and_bob.com') 

INSERT INTO Abstract (AbstractName, [Description], Keywords) VALUES
	('Unicorn Studies', 'Life of unicorns', 'unicorn,life,magic'),
	('Wizard History', 'Great accomplishments during wizard era', 'wizard,magic,history')

INSERT INTO Paper (Title, PaperUrl, AbstractId) VALUES 
	('Research into Unicorn Studies', 'https://docs.google.com/document/d/1pMPK-X5hh7LDVxoS_sQ5NClLtLDXvR7mI9-pUZ8aVxE/edit?usp=sharing', 1), 
	('Research into Wizard History', 'https://docs.google.com/document/d/1pMPK-X5hh7LDVxoS_sQ5NClLtLDXvR7mI9-pUZ8aVxE/edit?usp=sharing', 2)

INSERT INTO Bid(AbstractId,PCId,[Status]) VALUES (1,1,'Please'),(1,2,'Could'),(2,1,'Refuse') 
delete from Bid where [Status] = 'refuse'
INSERT INTO Proposal(AbstactId,MetaInfo) VALUES (1,'Meta-info about Unicorn Studies'),(2,'Meta-info about Wizard History')

/* REVIEWERS AND EVALUATIONS */

SELECT * FROM PCMember
SELECT * FROM Abstract
SELECT * FROM Paper
SELECT * FROM Evaluation
SELECT * FROM Bid
SELECT * FROM Proposal
SELECT * FROM Reviewer
SELECT * FROM Author 
SELECT * FROM [Session]
SELECT * FROM Conference
SELECT * FROM AuthorDeadlines
SELECT * FROM EvaluationDeadlines
SELECT * FROM Ticket
SELECT * FROM [Listener] WHERE Email = 'a' 
SELECT * FROM Author

/* Make johnny and alice reviewers */
INSERT INTO Reviewer (PCId) VALUES (1), (2)

/* Make johnny reviewer of Unicorn Studies and Wizard Studies and give alice ony the unicorns */
INSERT INTO Evaluation (PaperId,ReviewerId) VALUES (1,1), (2,1), (1,2) 
INSERT INTO Evaluation (PaperId,ReviewerId) VALUES (1,4), (2,4)


INSERT INTO [Listener](Email, [Password]) VALUES ('John Doe','bambi') 
--INSERT INTO Ticket(SessionId,ListenerId) VALUES(1,1)
INSERT INTO Author(FirstName,LastName,Email,Affiliation,[Password]) VALUES('Joanne','Rowling','jkrowling@gmail.com','writer','mischief')
INSERT INTO AuthorDeadlines(AbstractDeadline,PaperDeadline) VALUES ('2019-01-01', '2019-03-03')
INSERT INTO EvaluationDeadlines(BiddingDeadline,ReviewingDeadline) VALUES ('2019-04-04','2019-05-05')
INSERT INTO Conference(ChairId,Name,Date,SecretKey) VALUES(1,'Fantastic Beasts and where to find them','2019-05-05','magic')
INSERT INTO [Session](SessionName,ConferenceId,SessionChairId,SpeakerId,RoomId,[Time]) VALUES('Magic Session',1,1,1,1,'12:30 A.M')
/*
INSERT INTO Evaluation(PaperId,ReviewerId) VALUES (2,2)
DELETE FROM Evaluation WHERE PaperId =2 AND ReviewerId =2

UPDATE Bid SET [Status] = 'Refuse' WHERE AbstractId = 2 AND PCId = 1
DELETE FROM Bid WHERE AbstractId = 2 AND PCId = 1
SELECT * FROM Bid WHERE AbstractId = 2 AND PCId = 1
*/


/* get assigned papers */
SELECT *
FROM Evaluation E INNER JOIN Paper P ON E.PaperId = P.PaperId
WHERE ReviewerId = 1

/* update paper evaluation */
UPDATE	Evaluation 
SET Mark = 'ceva', Recommendation = 'mai mult text'
WHERE PaperId = 1 and ReviewerId = 2

/*All the Papers that went through the bidding stage */
SELECT *
FROM Paper
WHERE PaperId IN(
	SELECT DISTINCT P.PaperId 
	FROM Bid B INNER JOIN Paper P ON B.AbstractId = P.AbstractId
	WHERE B.[Status] != 'Refuse')

SELECT * FROM Conference
Delete from Conference
SELECT ConfId, ChairId, [Name], [Date], SecretKey FROM Conference WHERE ConfId = 1
SELECT TOP 1 ConfId FROM Conference 

SELECT A.AbstractId, AbstractName, [Description], Keywords 
FROM Abstract A INNER JOIN Contribution C on A.AbstractId = C.AbstractId
WHERE C.AuthorId = 2

select * from Abstract
select * from Contribution

INSERT INTO Contribution (AuthorId, AbstractId) VALUES (@AuthorId, @AbstractId)

delete from Contribution where AbstractId = 10
delete from Abstract where AbstractId = 10

select * from Paper
select * from Reviewer
select * from PCMember