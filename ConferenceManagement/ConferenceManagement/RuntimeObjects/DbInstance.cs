﻿using ConferenceManagement.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ConferenceManagement.RuntimeObjects
{
    public sealed class DbInstance
    {
        private static DbInstance instance = new DbInstance();
        
        private DbInstance()
        {
            
        }

        public static DbInstance GetDbInstance()
        {
            return instance;
        }

        public List<Paper> getAllPapers()
        {
            String statement = "SELECT * FROM Paper";
            List<Paper> papers = new List<Paper>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";

            //String conn = "Data Source = DESKTOP-H67RC4R\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Paper paper = new Paper();
                    paper.Id = sqlDataReader.GetInt32(0);
                    paper.Title = sqlDataReader.GetString(1);
                    paper.Status = sqlDataReader.GetString(2);
                    paper.PaperUrl = sqlDataReader.GetString(3);
                    int abstractId = sqlDataReader.GetInt32(4);
                    paper.Abstract = getAbstract(abstractId);
                    papers.Add(paper);
                }
                Console.WriteLine(papers);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return papers;
        }

        internal void RegisterPC(ProgramCommittee pc)
        {
            string sqlQuery = "INSERT INTO PCMember (Username,[Password],FirstName,LastName,Email,Affiliation,WebPage) VALUES" +
                                "(@Username, @Password, @FirstName, @LastName, @Email, @Affiliation, @Webpage)";
            
            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);

            try
            {
                cmd.Parameters.Add(new SqlParameter("@Username", pc.Username));
                cmd.Parameters.Add(new SqlParameter("@Password", pc.Password));
                cmd.Parameters.Add(new SqlParameter("@FirstName", pc.FirstName));
                cmd.Parameters.Add(new SqlParameter("@LastName", pc.LastName));
                cmd.Parameters.Add(new SqlParameter("@Email", pc.Email));
                cmd.Parameters.Add(new SqlParameter("@Affiliation", pc.Affiliation));
                cmd.Parameters.Add(new SqlParameter("@Webpage", pc.WebPage));
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

        internal void RegisterAuthor(Author author)
        {
            string sqlQuery = "INSERT INTO Author (FirstName, LastName, Email, Affiliation, [Password]) VALUES" +
                                "(@FirstName, @LastName, @Email, @Affiliation, @Password)";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);

            try
            {
                cmd.Parameters.Add(new SqlParameter("@FirstName", author.firstName));
                cmd.Parameters.Add(new SqlParameter("@LastName", author.lastName));
                cmd.Parameters.Add(new SqlParameter("@Email", author.email));
                cmd.Parameters.Add(new SqlParameter("@Affiliation", author.affiliation));
                cmd.Parameters.Add(new SqlParameter("@Password", author.password));
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

        internal void RegisterListener(Listener listener)
        {
            string sqlQuery = "INSERT INTO Listener (Username, [Password]) VALUES" +
                                "(@Username, @Password)";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);

            try
            {
                cmd.Parameters.Add(new SqlParameter("@Username", listener.Email));
                cmd.Parameters.Add(new SqlParameter("@Password", listener.Password));
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

        internal void AddSession(Session session)
        {
            string sqlQuery;
            if (true)
                sqlQuery = "INSERT INTO [Session](SessionName,ConferenceId,SessionChairId,SpeakerId,RoomId,[Time])" +
                            " VALUES(@SessionName,@ConfId,@ChairId,@SpeakerId,@RoomId,@Time)";
            else
                sqlQuery = "UPDATE EvaluationDeadlines SET BiddingDeadline = @BiddingDeadline, ReviewingDeadline = @ReviewDeadline" +
                            " WHERE ConfId = @ConfId";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);

            try
            {
                cmd.Parameters.Add(new SqlParameter("@SessionName", session.SessionName));
                cmd.Parameters.Add(new SqlParameter("@ConfId", session.ConferenceId));
                cmd.Parameters.Add(new SqlParameter("@ChairId", session.SessionChairId));
                cmd.Parameters.Add(new SqlParameter("@SpeakerId", session.SpeakerId));
                cmd.Parameters.Add(new SqlParameter("@RoomId", session.RoomId));
                cmd.Parameters.Add(new SqlParameter("@Time", session.Time));
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

        internal List<Abstract> GetAuthorAbstracts(long authorId)
        {
            String statement = "SELECT A.AbstractId, AbstractName, [Description], Keywords " + 
                                " FROM Abstract A INNER JOIN Contribution C on A.AbstractId = C.AbstractId" +
                                " WHERE C.AuthorId = @AuthorId";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@AuthorId", authorId));

            List<Abstract> abstracts = new List<Abstract>();
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while(sqlDataReader.Read())
                {
                    Abstract Abstract = new Abstract();
                    Abstract.Id = sqlDataReader.GetInt32(0);
                    Abstract.Name = sqlDataReader.GetString(1);
                    Abstract.Description = sqlDataReader.GetString(2);
                    Abstract.Keywords = sqlDataReader.GetString(3);
                    abstracts.Add(Abstract);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return abstracts;
        }

        internal bool IsReviewer(long PCId)
        {
            String statement = "SELECT COUNT(*) FROM Reviewer WHERE PCId = @PCId";
            
            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            Boolean result = false;
            try
            {
                cmd.Parameters.Add(new SqlParameter("@PCId", PCId));
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                sqlDataReader.Read();
                int count = sqlDataReader.GetInt32(0);
                if (count == 1) result = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return result;
        }

        internal long GetLastConferenceId()
        {
            string sqlQuery = "SELECT TOP 1 ConfId FROM Conference";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);

            ProgramCommittee ConferenceChair = MockInstance.GetDbInstance().getCurrentProgramCommitee();
            long LastConferenceId = -1;
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                if (sqlDataReader.Read())
                {
                    LastConferenceId = sqlDataReader.GetInt32(0);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
            return LastConferenceId;
        }

        internal void AddEvaluationDeadlines(EvaluationDeadlines evaluationDeadlines)
        {
            string sqlQuery;
            if (!DeadlineExists("Evaluation", evaluationDeadlines.ConfId))
                sqlQuery = "INSERT INTO EvaluationDeadlines(ConfId, BiddingDeadline, ReviewingDeadline)" +
                                  " VALUES (@ConfId, @BiddingDeadline, @ReviewDeadline)";
            else
                sqlQuery = "UPDATE EvaluationDeadlines SET BiddingDeadline = @BiddingDeadline, ReviewingDeadline = @ReviewDeadline" +
                            " WHERE ConfId = @ConfId";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);

            ProgramCommittee ConferenceChair = MockInstance.GetDbInstance().getCurrentProgramCommitee();

            try
            {
                cmd.Parameters.Add(new SqlParameter("@ConfId", evaluationDeadlines.ConfId));
                cmd.Parameters.Add(new SqlParameter("@BiddingDeadline", evaluationDeadlines.BiddingDeadline));
                cmd.Parameters.Add(new SqlParameter("@ReviewDeadline", evaluationDeadlines.ReviewDeadline));
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

        internal void AddAuthorDeadlines(AuthorDeadlines authorDeadlines)
        {
            string sqlQuery;
            if (!DeadlineExists("Author", authorDeadlines.ConfId))
                sqlQuery = "INSERT INTO AuthorDeadlines(ConfId, AbstractDeadline, PaperDeadline)" +
                             " VALUES (@ConfId, @AbstractDeadline, @PaperDeadline)";
            else
                sqlQuery = "UPDATE AuthorDeadlines SET AbstractDeadline = @AbstractDeadline, PaperDeadline = @PaperDeadline" +
                            " WHERE ConfId = @ConfId";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);

            ProgramCommittee ConferenceChair = MockInstance.GetDbInstance().getCurrentProgramCommitee();

            try
            {
                cmd.Parameters.Add(new SqlParameter("@ConfId", authorDeadlines.ConfId));
                cmd.Parameters.Add(new SqlParameter("@AbstractDeadline", authorDeadlines.AbstractDeadline));
                cmd.Parameters.Add(new SqlParameter("@PaperDeadline", authorDeadlines.PaperDeadline));
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

        public List<Paper> GetAllBidAcceptedPapers()
        {
            String statement = "SELECT * FROM Paper WHERE PaperId IN( SELECT DISTINCT P.PaperId FROM Bid B INNER JOIN Paper P ON B.AbstractId = P.AbstractId WHERE B.[Status] != 'refuse')";
            List<Paper> papers = new List<Paper>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-H67RC4R\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Paper paper = new Paper();
                    paper.Id = sqlDataReader.GetInt32(0);
                    paper.Title = sqlDataReader.GetString(1);
                    paper.Status = sqlDataReader.GetString(2);
                    paper.PaperUrl = sqlDataReader.GetString(3);
                    int abstractId = sqlDataReader.GetInt32(4);
                    paper.Abstract = getAbstract(abstractId);
                    papers.Add(paper);
                }
                Console.WriteLine(papers);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return papers;
        }

        public List<Evaluation> getPaperEvaluations(long PaperId)
        {
            String statement = "SELECT PaperId, ReviewerId, Mark, Recommendation" +
                                " FROM Evaluation E" +
                                " WHERE PaperId = @PaperId";
            List<Evaluation> evaluations = new List<Evaluation>();

            String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@PaperId", PaperId));

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Evaluation evaluation = new Evaluation();

                    int readPaperId = sqlDataReader.GetInt32(0);
                    evaluation.Paper = this.getPaper(readPaperId);

                    int readReviewerId = sqlDataReader.GetInt32(1);
                    evaluation.Reviewer = this.getReviewer(readReviewerId);

                    string readMark = sqlDataReader.GetString(2);
                    evaluation.Mark = new Mark(readMark);

                    string readRecomm = sqlDataReader.GetString(3);
                    evaluation.Recommendation = readRecomm;

                    evaluations.Add(evaluation);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return evaluations;
        }

        public List<Paper> getAssignedPapers(long PCId)
        {
            String statement = "SELECT P.PaperId" +
                                " FROM Evaluation E INNER JOIN Paper P ON E.PaperId = P.PaperId" +
                                " WHERE ReviewerId = " + PCId;
            List<Paper> papers = new List<Paper>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-H67RC4R\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";

            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Paper paper = this.getPaper(sqlDataReader.GetInt32(0));
                    papers.Add(paper);
                }
                Console.WriteLine(papers);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");

                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return papers;
        }

        public Evaluation getEvaluation(long ReviewerId, long PaperId)
        {
            String statement = "SELECT PaperId, ReviewerId, Mark, Recommendation FROM Evaluation WHERE PaperId = @PaperId and ReviewerId = @ReviewerId";
            Evaluation evaluation = new Evaluation();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-H67RC4R\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@PaperId", PaperId));
            cmd.Parameters.Add(new SqlParameter("@ReviewerId", ReviewerId));

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                sqlDataReader.Read();
                int readPaperId = sqlDataReader.GetInt32(0);
                evaluation.Paper = this.getPaper(readPaperId);

                int readReviewerId = sqlDataReader.GetInt32(1);
                evaluation.Reviewer = this.getReviewer(readReviewerId);

                string readMark = sqlDataReader.GetString(2);
                evaluation.Mark = new Mark(readMark);

                string readRecomm = sqlDataReader.GetString(3);
                evaluation.Recommendation = readRecomm;
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return evaluation;
        }

        public Reviewer getReviewer(long reviewerId)
        {
            String statement = "SELECT R.PCId, Username, Password, FirstName, LastName, Email, Affiliation, WebPage " +
                " FROM Reviewer R INNER JOIN PCMember PC ON R.PCId = PC.PCId WHERE R.PCId = " + reviewerId;
            Reviewer reviewer = new Reviewer();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-H67RC4R\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    reviewer.PCId = sqlDataReader.GetInt32(0);
                    reviewer.Username = sqlDataReader.GetString(1);
                    reviewer.Password = sqlDataReader.GetString(2);
                    reviewer.FirstName = sqlDataReader.GetString(3);
                    reviewer.LastName = sqlDataReader.GetString(4);
                    reviewer.Email = sqlDataReader.GetString(5);
                    reviewer.Affiliation = sqlDataReader.GetString(6);
                    reviewer.WebPage = sqlDataReader.GetString(7);
                }
                Console.WriteLine(reviewer);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally

            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return reviewer;
        }

        public ProgramCommittee GetProgramCommittee(long PCId)
        {
            String statement = "SELECT * FROM PCMember where PCId = " + PCId;
            ProgramCommittee programCommittee = new ProgramCommittee();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-H67RC4R\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";

            //String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    programCommittee.PCId = sqlDataReader.GetInt32(0);
                    programCommittee.Username = sqlDataReader.GetString(1);
                    programCommittee.Password = sqlDataReader.GetString(2);
                    programCommittee.FirstName = sqlDataReader.GetString(3);
                    programCommittee.LastName = sqlDataReader.GetString(4);
                    programCommittee.Email = sqlDataReader.GetString(5);
                    programCommittee.Affiliation = sqlDataReader.GetString(6);
                    programCommittee.WebPage = sqlDataReader.GetString(7);
                }
                Console.WriteLine(programCommittee);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return programCommittee;
        }

        public Abstract getAbstract(long id)
        {
            String statement = "SELECT * FROM Abstract WHERE AbstractId = " + id;
            Abstract SqlAbstract = new Abstract();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-H67RC4R\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    SqlAbstract.Id = sqlDataReader.GetInt32(0);
                    SqlAbstract.Name = sqlDataReader.GetString(1);
                    SqlAbstract.Description = sqlDataReader.GetString(2);
                    SqlAbstract.Keywords = sqlDataReader.GetString(3);
                }
                Console.WriteLine(SqlAbstract);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return SqlAbstract;
        }

        public Paper getPaper(long id)
        {
            String statement = "SELECT * FROM Paper WHERE PaperId = " + id;
            Paper paper = new Paper();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-H67RC4R\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    paper.Id = sqlDataReader.GetInt32(0);
                    paper.Title = sqlDataReader.GetString(1);
                    paper.Status = sqlDataReader.GetString(2);
                    paper.PaperUrl = sqlDataReader.GetString(3);
                }
                Console.WriteLine(paper);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return paper;
        }

        public void updatePaper(long reviewerId, long paperId, string mark, string recommendation)
        {
            String statement = "UPDATE Evaluation SET Mark = @Mark, Recommendation = @Recommendation WHERE PaperId = @PaperId and ReviewerId = @ReviewerId";

            Console.WriteLine(statement);

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-H67RC4R\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@Mark", mark));
            cmd.Parameters.Add(new SqlParameter("@Recommendation", recommendation));
            cmd.Parameters.Add(new SqlParameter("@PaperId", paperId));
            cmd.Parameters.Add(new SqlParameter("@ReviewerId", reviewerId));

            cmd.ExecuteNonQuery();

            sqlConnection.Close();
        }

        public Conference GetConference(long confId)
        {
            String statement = "SELECT ConfId, ChairId, [Name], [Date], SecretKey" +
                                " FROM Conference WHERE ConfId = @ConfId";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@ConfId", confId));

            Conference conference = new Conference();
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    conference.ConfId = sqlDataReader.GetInt32(0);
                    int ChairId = sqlDataReader.GetInt32(1);
                    conference.Chair = this.GetProgramCommittee(ChairId);
                    conference.Name = sqlDataReader.GetString(2);
                    conference.DateTime = sqlDataReader.GetDateTime(3);
                    conference.SecretKey = sqlDataReader.GetString(4);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return conference;
        }

        public AuthorDeadlines GetAuthorDeadlines(long deadlineId)
        {
            String statement = "SELECT DId, AbstractDeadline, PaperDeadline" +
                                " FROM AuthorDeadlines WHERE DId = @DId";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@DId", deadlineId));

            AuthorDeadlines authorDeadline = new AuthorDeadlines();
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                sqlDataReader.Read();

                int DId = sqlDataReader.GetInt32(0);
                authorDeadline.DId = DId;
                authorDeadline.AbstractDeadline = sqlDataReader.GetDateTime(1);
                authorDeadline.PaperDeadline = sqlDataReader.GetDateTime(1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return authorDeadline;
        }

        public AuthorDeadlines GetAuthorDeadlinesByConf(long confId)
        {
            String statement = "SELECT DId, AbstractDeadline, PaperDeadline" +
                                " FROM AuthorDeadlines WHERE ConfId = @ConfId";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@ConfId", confId));

            AuthorDeadlines authorDeadline = new AuthorDeadlines();
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                sqlDataReader.Read();

                int DId = sqlDataReader.GetInt32(0);
                authorDeadline.DId = DId;
                authorDeadline.AbstractDeadline = sqlDataReader.GetDateTime(1);
                authorDeadline.PaperDeadline = sqlDataReader.GetDateTime(2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return authorDeadline;
        }

        public EvaluationDeadlines GetEvaluationDeadlines(long deadlineId)
        {
            String statement = "SELECT DId, BiddingDeadline, ReviewingDeadline" +
                                " FROM EvaluationDeadlines WHERE DId = @DId";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@DId", deadlineId));

            EvaluationDeadlines evaluationDeadlines = new EvaluationDeadlines();
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                sqlDataReader.Read();

                int DId = sqlDataReader.GetInt32(0);
                evaluationDeadlines.DId = DId;
                evaluationDeadlines.BiddingDeadline = sqlDataReader.GetDateTime(1);
                evaluationDeadlines.ReviewDeadline = sqlDataReader.GetDateTime(2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return evaluationDeadlines;
        }

        public EvaluationDeadlines GetEvaluationDeadlinesByConf(long confId)
        {
            String statement = "SELECT DId, BiddingDeadline, ReviewingDeadline" +
                               " FROM EvaluationDeadlines WHERE ConfId = @ConfId";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@ConfId", confId));

            EvaluationDeadlines evaluationDeadlines = new EvaluationDeadlines();
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                sqlDataReader.Read();

                int DId = sqlDataReader.GetInt32(0);
                evaluationDeadlines.DId = DId;
                evaluationDeadlines.BiddingDeadline = sqlDataReader.GetDateTime(1);
                evaluationDeadlines.ReviewDeadline = sqlDataReader.GetDateTime(2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return evaluationDeadlines;
        }

        public void AddConference(Conference conference)
        {
            string sqlQuery = "INSERT INTO Conference(ChairId, [Name], [Date], SecretKey)" +
                " VALUES (@ChairId, @Name, @Date, @SecretKey)";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);

            ProgramCommittee ConferenceChair = MockInstance.GetDbInstance().getCurrentProgramCommitee();

            try
            {
                cmd.Parameters.Add(new SqlParameter("@ChairId", ConferenceChair.PCId));
                cmd.Parameters.Add(new SqlParameter("@Name", conference.Name));
                cmd.Parameters.Add(new SqlParameter("@Date", conference.DateTime));
                cmd.Parameters.Add(new SqlParameter("@SecretKey", conference.SecretKey));
                Console.WriteLine(cmd);
                cmd.ExecuteNonQuery();
                MockInstance.GetDbInstance().ResetCurrentConference();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

        public IEnumerable<Conference> getAllConferences()
        {
            String statement = "SELECT ConfId, ChairId, [Name], [Date], SecretKey FROM Conference";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();

            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            List<Conference> conferences = new List<Conference>();
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Conference conference = new Conference();
                    conference.ConfId = sqlDataReader.GetInt32(0);
                    int ChairId = sqlDataReader.GetInt32(1);
                    conference.Chair = this.GetProgramCommittee(ChairId);
                    conference.Name = sqlDataReader.GetString(2);
                    conference.DateTime = sqlDataReader.GetDateTime(3);
                    conference.SecretKey = sqlDataReader.GetString(4);
                    conferences.Add(conference);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return conferences;
        }

        internal Boolean DeadlineExists(string evaluationType, long ConfId)
        {
            String statement;
            if (evaluationType == "Author")
                statement = "SELECT COUNT(*) FROM AuthorDeadlines WHERE ConfId = @ConfId";
            else
                statement = "SELECT COUNT(*) FROM EvaluationDeadlines WHERE ConfId = @ConfId";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            Boolean result = false;
            try
            {
                cmd.Parameters.Add(new SqlParameter("@ConfId", ConfId));
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                sqlDataReader.Read();
                int count = sqlDataReader.GetInt32(0);
                if (count == 1) result = true; 
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return result;
        }

        public List<Author> GetAllAuthors()
        {
            String statement = "SELECT * FROM Author";
            List<Author> authors = new List<Author>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Author author = new Author();
                    author.authorId = sqlDataReader.GetInt32(0);
                    author.firstName = sqlDataReader.GetString(1);
                    author.lastName = sqlDataReader.GetString(2);
                    author.email = sqlDataReader.GetString(3);
                    author.affiliation = sqlDataReader.GetString(4);
                    author.password = sqlDataReader.GetString(5);
                    authors.Add(author);
                }
                Console.WriteLine(authors);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return authors;
        }

        public List<ProgramCommittee> GetAllProgramComittees()
        {
            String statement = "SELECT * FROM PCMember";
            List<ProgramCommittee> pcMembers = new List<ProgramCommittee>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    ProgramCommittee programCommittee = new ProgramCommittee();
                    programCommittee.PCId = sqlDataReader.GetInt32(0);
                    programCommittee.Username = sqlDataReader.GetString(1);
                    programCommittee.Password = sqlDataReader.GetString(2);
                    programCommittee.FirstName = sqlDataReader.GetString(3);
                    programCommittee.LastName = sqlDataReader.GetString(4);
                    programCommittee.Email = sqlDataReader.GetString(5);
                    programCommittee.Affiliation = sqlDataReader.GetString(6);
                    programCommittee.WebPage = sqlDataReader.GetString(7);
                    pcMembers.Add(programCommittee);
                }
                Console.WriteLine(pcMembers);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return pcMembers;
        }

        public List<Listener> GetAllListeners()
        {
            String statement = "SELECT * FROM Listener";
            List<Listener> listeners = new List<Listener>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Listener listener = new Listener();
                    listener.ListenerId = sqlDataReader.GetInt32(0);
                    listener.Email = sqlDataReader.GetString(1);
                    listener.Password = sqlDataReader.GetString(2);
                    listeners.Add(listener);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return listeners;
        }
    }
}