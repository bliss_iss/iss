﻿using ConferenceManagement.Models;
using System.Collections.Generic;
using System.Linq;

namespace ConferenceManagement.RuntimeObjects
{
    public sealed class MockInstance
    {
        private static MockInstance instance = new MockInstance();
        private static long CurrentConferenceId;
        private static ProgramCommittee ProgramCommittee = null;
        private static Reviewer Reviewer = null;
        private static Author Author = null;
        private static Listener Listener = null;

        private MockInstance()
        {
            IEnumerable<Conference> conferences = DbInstance.GetDbInstance().getAllConferences();
            if (conferences.Count() > 0)
            {
                MockInstance.CurrentConferenceId = conferences.First().ConfId;
            }
        }

        public static MockInstance GetDbInstance()
        {
            return instance;
        }
        
        public Reviewer getCurrentReviewer()
        {
            return Reviewer;
            //return DbInstance.GetDbInstance().getReviewer(1);
        }

        public ProgramCommittee getCurrentProgramCommitee()
        {
            return ProgramCommittee;
            //return DbInstance.GetDbInstance().GetProgramCommittee(1);
        }

        public Listener getCurrentListener()
        {
            return Listener;
        }

        public Author getCurrentAuthor()
        {
            return Author;
        }

        internal void LoginAuthor(string email, string password)
        {
            ResetLogin();
            List<Author> authors = DbInstance.GetDbInstance().GetAllAuthors();
            Author = authors.Find(a => a.email.Equals(email) && a.password.Equals(password));
        }

        internal void LoginPC(string email, string password)
        {
            ResetLogin();
            List<ProgramCommittee> programCommittees = DbInstance.GetDbInstance().GetAllProgramComittees();
            ProgramCommittee = programCommittees.Find(pc => pc.Email.Equals(email) && pc.Password.Equals(password));
            LoginReviewer(ProgramCommittee.PCId);
        }

        internal void LoginListener(string email, string password)
        {
            ResetLogin();
            List<Listener> listeners = DbInstance.GetDbInstance().GetAllListeners();
            Listener = listeners.Find(l => l.Email.Equals(email) && l.Password.Equals(password));
        }

        internal void LoginReviewer(long PCId)
        {
            if (DbInstance.GetDbInstance().IsReviewer(PCId))
                Reviewer = new Reviewer(ProgramCommittee);
        }

        public void ResetLogin()
        {
            ProgramCommittee = null;
            Reviewer = null;
            Author = null;
            Listener = null;
        }

        public Conference getCurrentConference()
        {
            return DbInstance.GetDbInstance().GetConference(MockInstance.CurrentConferenceId);
        }

        public void SetCurrentConference(long id)
        {
            MockInstance.CurrentConferenceId = id;
        }

        public void ResetCurrentConference()
        {
            long LatestConferenceId = DbInstance.GetDbInstance().GetLastConferenceId();
            MockInstance.CurrentConferenceId = LatestConferenceId;
        }

        public string GetCurrentEmail()
        {
            if (ProgramCommittee != null)
                return "Welcome, pc " + ProgramCommittee.Email;
            if (Author != null)
                return "Welcome, Author " + Author.email;
            if (Reviewer != null)
                return "Welcome, Reviewer " + Reviewer.Email;
            if (Listener != null)
                return "Welcome, Listener " + Listener.Email;
            return "";
        }
    }
}