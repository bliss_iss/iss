﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Reviewer : ProgramCommittee
    {
        public Reviewer() { }

        public Reviewer(ProgramCommittee pc)
            : base(pc.PCId, pc.Username, pc.Password, pc.LastName, pc.FirstName, pc.WebPage, pc.Affiliation, pc.Email)
        { }

        public Reviewer(long pcID, string username, string password, string lastName, string firstName, string webPage, string affiliation, string email)
             : base(pcID, username, password, lastName, firstName, webPage, affiliation, email)
        { }

        private void evaluatePaper(Paper paper)
        {
            throw new Exception("Not yet implemented");
        }

        private List<Evaluation> seeOtherEvaluations()
        {
            throw new Exception("Not yet implemented");
        }
    }
}