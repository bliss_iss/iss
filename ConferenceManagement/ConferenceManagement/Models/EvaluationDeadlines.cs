﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class EvaluationDeadlines
    {
        public long DId { get; set; }
        public long ConfId { get; set; }
        public DateTime BiddingDeadline { get; set; }
        public DateTime ReviewDeadline { get; set; }
    }
}