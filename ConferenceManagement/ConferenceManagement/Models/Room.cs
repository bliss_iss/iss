﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Room
    {
        public long RoomId { get; set; }
        public string Name{ get; set; }
        public int Capacity { get; set; }
    }
}