﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class AuthorDeadlines
    {
        public long DId { get; set; }
        public long ConfId { get; set; }
        public DateTime AbstractDeadline { get; set; }
        public DateTime PaperDeadline { get; set; }
    }
}