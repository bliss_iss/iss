﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Cochair : ProgramCommittee
    {
        public Cochair(long pcID, string username, string password, string lastName, string firstName, string webPage, string affiliation, string email)
             : base(pcID, username, password, lastName, firstName, webPage, affiliation, email)
        { }

        public string changeDeadline(string deadline)
        {
            throw new Exception("Not yet implemented");
        }

        public void assignPaper(List<Paper> papers, List<Reviewer> reviewers)
        {
            throw new Exception("Not yet implemented");
        }

        public void sendResult(Paper paper)
        {
            throw new Exception("Not yet implemented");
        }
    }
}