﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Author
    {

        public Author() { }
        public Author(long authorId, string firstName, string lastName, string email, string affiliation, string password)
        {
            this.authorId = authorId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.affiliation = affiliation;
            this.password = password;
        }

        public long authorId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string affiliation { get; set; }
        public string password { get; set; }
    }
}