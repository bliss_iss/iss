﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Session
    {
        public long SessionId { get; set; }
        public string SessionName { get; set; }
        public long ConferenceId { get; set; }
        public long SessionChairId { get; set; }
        public long SpeakerId { get; set; }
        public long RoomId { get; set; }
        public string Time { get; set; }

        public ProgramCommittee ProgramCommittee { get; set; }
        public Author Author { get; set; }
        public Room Room { get; set; }

        //public Cochair sessionChair { get; set; }
        //public List<Speaker> speakers { get; set; }
    }
}