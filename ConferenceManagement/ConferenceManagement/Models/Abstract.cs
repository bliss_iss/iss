﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Abstract
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public List<Author> Authors { get; set; }   

        public Abstract()
        {

        }

        public Abstract(Abstract Abstract)
        {
            Id = Abstract.Id;
            Name = Abstract.Name;
            Description = Abstract.Name;
            Keywords = Abstract.Keywords;
        }

        public Abstract(long id, string name, string description, string keywords, List<Author> authors)
        {
            Id = id;
            Name = name;
            Description = description;
            this.Keywords = keywords;
            this.Authors = authors;
        }
    }
}