﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Mark
    {
        public string value;
        public static List<String> marks = new List<String> { "Strong reject", "Reject", "Weak reject", "Borderline", "Weak accept", "Accept", "Strong accept" };
        
        public Mark(string val)
        {
            this.value = marks.Find(mark => mark == val);
            if (this.value == null)
                this.value = "NOT PROCESSED";
        }
    }
}