﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Paper
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public string PaperUrl { get; set; }
        public Abstract Abstract { get; set; }
        public long AbstractId { get; set; }

        public Paper() { }

        public Paper(int id, string title, string status, string paperUrl, Abstract AbstractPaper)
        {
            Id = id;
            Title = title;
            Status = status;
            PaperUrl = paperUrl;
            Abstract = AbstractPaper;
        }
    }
}