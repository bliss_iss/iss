﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Evaluation
    {
        public Paper Paper { get; set; }
        public Reviewer Reviewer { get; set; }
        public Mark Mark { get; set; }
        public long PaperId { get; set; }
        public long ReviewerId { get; set; }
        public string Recommendation { get; set; }
    }
}