﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class ProgramCommittee
    {
        public long PCId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string WebPage { get; set; }
        public string Affiliation { get; set; }
        public string Email { get; set; }
        public string SecretKey { get; set; }

        public ProgramCommittee()
        {
        }

        public ProgramCommittee(long pcID, string username, string password, string lastName, string firstName, string webPage, string affiliation, string email)
        {
            this.PCId = pcID;
            this.Username = username;
            this.Password = password;
            this.LastName = lastName;
            this.FirstName = firstName;
            this.WebPage = webPage;
            this.Affiliation = affiliation;
            this.Email = email;
        }

        public void register(string lastName, string firstName, string email, string password, string affiliation, string webPage, string username)
        {
            throw new Exception("Not yet implemented");
        }

        public Mark bidProposal(Proposal proposal, string deadline)
        {
            throw new Exception("Not yet implemented");
        }

        public void postEditionInfo(string name, List<String> eventDays, string callForPapers, List<string> deadlines, List<ProgramCommittee> programCommittees, List<String> conferenceSections)
        {
            throw new Exception("Not yet implemented");
        }
        
        public void classifyPapers(List<Paper> papers)
        {
            throw new Exception("Not yet implemented");
        }

        public void decideStructure(List<Paper> acceptedPapers, long numberOfParticipants = 0)
        {
            throw new Exception("Not yet implemented");
        }

        public void assignSessionChairs(List<Session> sessions, List<ProgramCommittee> programCommittees)
        {
            throw new Exception("Not yet implemented");
        }
    }
}