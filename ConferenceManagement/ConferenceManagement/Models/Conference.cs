﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Conference
    {
        public long ConfId { get; set; }
        public ProgramCommittee Chair { get; set; }
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateTime { get; set; }

        public AuthorDeadlines AuthorDeadlines { get; set; }
        public EvaluationDeadlines EvaluationDeadlines { get; set; }
        public string SecretKey { get; set; }
    }
}