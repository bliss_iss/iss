﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceManagement.Models
{
    public class Bid
    {
        public long AbstractId { get; set; }
        public long ProgramComitteeId { get; set; }
        public string Status { get; set; }
        public Abstract AbstractPaper { get; set; }
        public ProgramCommittee ProgramCommittee { get; set; }

        public Bid() { }
    }
}