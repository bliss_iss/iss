﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConferenceManagement.Models;
using System.Data.SqlClient;

namespace ConferenceManagement.Repository
{
    public class ProposalDBControl
    {
        public List<Proposal> GetAllProposals()
        {
            String statement = "SELECT * FROM Proposal";
            List<Proposal> proposals = new List<Proposal>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-H67RC4R\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            //String conn = "Data Source = DESKTOP-4JCIPS9\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Proposal proposal = new Proposal();
                    proposal.AbstractId = sqlDataReader.GetInt32(0);
                    proposal.MetaInfo = sqlDataReader.GetString(1);
                    proposals.Add(proposal);
                }
                Console.WriteLine(proposals);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return proposals;
        }

        internal void CreateContribution(long authorId, long AbstractId)
        {
            string sqlQuery = "INSERT INTO Contribution (AuthorId, AbstractId) VALUES (@AuthorId, @AbstractId)";
            
            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);
                cmd.Parameters.Add(new SqlParameter("@AuthorId", authorId));
                cmd.Parameters.Add(new SqlParameter("@AbstractId", AbstractId));

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

        public List<Abstract> GetAllAbstracts()
        {
            String statement = "SELECT * FROM Abstract";
            List<Abstract> abstractsFinal = new List<Abstract>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Abstract abstractPaper = new Abstract();
                    abstractPaper.Id = sqlDataReader.GetInt32(0);
                    abstractPaper.Name = sqlDataReader.GetString(1);
                    abstractPaper.Description = sqlDataReader.GetString(2);
                    abstractPaper.Keywords = sqlDataReader.GetString(3);
                    abstractPaper.Authors = GetAllContributionAuthors(abstractPaper.Id);
                    abstractsFinal.Add(abstractPaper);
                }
                Console.WriteLine(abstractsFinal);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return abstractsFinal;
        }

        public List<Author> GetAllContributionAuthors(long id)
        {
            String statement =  "Select * from Author " +
                                "INNER Join Contribution " +
                                "on Contribution.AuthorId = Author.AuthorId " +
                                "Where Contribution.AbstractId = @id";

            List<Author> authors = new List<Author>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@id", id));
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Author author = new Author();
                    author.authorId = sqlDataReader.GetInt32(0);
                    author.firstName = sqlDataReader.GetString(1);
                    author.lastName = sqlDataReader.GetString(2);
                    author.email = sqlDataReader.GetString(3);
                    author.affiliation = sqlDataReader.GetString(4);
                    authors.Add(author);
                }
                Console.WriteLine(authors);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return authors;
        }

        public List<Bid> GetAllBids()
        {
            String statement = "SELECT * FROM Bid";
            List<Bid> bids = new List<Bid>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Bid bid = new Bid();
                    bid.AbstractId = sqlDataReader.GetInt32(0);
                    bid.ProgramComitteeId = sqlDataReader.GetInt32(1);
                    bid.Status = sqlDataReader.GetString(2);
                    bids.Add(bid);
                }
                Console.WriteLine(bids);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return bids;
        }

        public List<Evaluation> GetAllEvaluations()
        {
            String statement = "SELECT * FROM Evaluation";
            List<Evaluation> evaluations = new List<Evaluation>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Evaluation evaluation = new Evaluation();
                    evaluation.PaperId = sqlDataReader.GetInt32(0);
                    evaluation.ReviewerId = sqlDataReader.GetInt32(1);
                    evaluations.Add(evaluation);
                }
                Console.WriteLine(evaluations);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return evaluations;
        }

        public List<Session> GetAllSessions()
        {
            String statement = "SELECT * FROM [Session]";
            List<Session> sessions = new List<Session>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Session session = new Session();
                    session.SessionId = sqlDataReader.GetInt32(0);
                    session.SessionName = sqlDataReader.GetString(1);
                    session.ConferenceId = sqlDataReader.GetInt32(2);
                    session.SessionChairId = sqlDataReader.GetInt32(3);
                    session.SpeakerId = sqlDataReader.GetInt32(4);
                    session.RoomId = sqlDataReader.GetInt32(5);
                    session.Time = sqlDataReader.GetString(6);
                    sessions.Add(session);
                }
                Console.WriteLine(sessions);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return sessions;
        }

        public void BidProposal(long PCId, long AbstractId, string status)
        {
            string sqlQuery = "INSERT INTO Bid(AbstractId,PCId,[Status]) VALUES (@Aid, @PCId, @Status)";

            Console.WriteLine(sqlQuery);

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            try
            {
                if (checkExistingBid(PCId,AbstractId) != 0)
                    sqlQuery = "UPDATE Bid SET [Status] = @Status WHERE AbstractId = @Aid AND PCId = @PCId";

                SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);
                cmd.Parameters.Add(new SqlParameter("@PCId", PCId));
                cmd.Parameters.Add(new SqlParameter("@Aid", AbstractId));
                cmd.Parameters.Add(new SqlParameter("@Status", status));

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

        public int checkExistingBid(long PCId, long AbstractId)
        {
            string statement = "SELECT Count(*) FROM Bid WHERE AbstractId = @Aid AND PCId = @PCId";
            
            Console.WriteLine(statement);

            int count = -1;
            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            SqlCommand verification = new SqlCommand(statement, sqlConnection);
            verification.Parameters.Add(new SqlParameter("@Aid", AbstractId));
            verification.Parameters.Add(new SqlParameter("@PCId", PCId));

            SqlDataReader sqlDataReader = verification.ExecuteReader();
            while (sqlDataReader.Read())
            {
                count = sqlDataReader.GetInt32(0);
            }
            return count;
        }

        public Abstract GetAbstractById(long id)
        {
            String statement = "SELECT * FROM Abstract where AbstractId=@Aid";
            Abstract abstractPaper = new Abstract();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@Aid", id));
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                if (sqlDataReader.Read())
                {
                    abstractPaper.Id = sqlDataReader.GetInt32(0);
                    abstractPaper.Name = sqlDataReader.GetString(1);
                    abstractPaper.Description = sqlDataReader.GetString(2);
                    abstractPaper.Keywords = sqlDataReader.GetString(3);
                    //get all authors from Contribution
                    //abstractPaper.Authors = 
                }
                Console.WriteLine(abstractPaper);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return abstractPaper;
        }

        public Session GetSession(long id)
        {
            String statement = "SELECT * FROM [Session] WHERE SessionId = @SID";
            Session session = new Session();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@SID", id));
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                if (sqlDataReader.Read())
                {
                    session.SessionId = sqlDataReader.GetInt32(0);
                    session.SessionName = sqlDataReader.GetString(1);
                    session.ConferenceId = sqlDataReader.GetInt32(2);
                    session.SessionChairId = sqlDataReader.GetInt32(3);
                    session.SpeakerId = sqlDataReader.GetInt32(4);
                    session.RoomId = sqlDataReader.GetInt32(5);
                    session.Time = sqlDataReader.GetString(6);
                }
                Console.WriteLine(session);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return session;
        }

        public static Abstract GetAbstractByIdStatic(long id)
        {
            String statement = "SELECT * FROM Abstract where AbstractId=@Aid";
            Abstract abstractPaper = new Abstract();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@Aid", id));
            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                if (sqlDataReader.Read())
                {
                    abstractPaper.Id = sqlDataReader.GetInt32(0);
                    abstractPaper.Name = sqlDataReader.GetString(1);
                    abstractPaper.Description = sqlDataReader.GetString(2);
                    abstractPaper.Keywords = sqlDataReader.GetString(3);
                    //get all authors from Contribution
                    //abstractPaper.Authors = 
                }
                Console.WriteLine(abstractPaper);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return abstractPaper;
        }

        public void AssignPaper(long PaperId, long ReviewerId)
        {
            string sqlQuery = "INSERT INTO Evaluation(PaperId, ReviewerId) VALUES(@PaperId, @ReviewerId)";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);
                cmd.Parameters.Add(new SqlParameter("@PaperId", PaperId));
                cmd.Parameters.Add(new SqlParameter("@ReviewerId", ReviewerId));
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

        public void CreateAbstract(long authorId, string abstractName, string description, string keyWords)
        {
            string statement = "Insert into Abstract(AbstractName, Description, Keywords) Values(@abstractName, @description, @keyWords)";
            //string statementLastInserted = "SELECT AbstractId FROM Abstract where AbstractId = @@IDENTITY";
            //string statementInsertContribution = "Insert into Contribution(AuthorId, AbstractId) Values(@authorId, @abstractId)";
            
            string conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(statement, sqlConnection);
                cmd.Parameters.Add(new SqlParameter("@abstractName", abstractName));
                cmd.Parameters.Add(new SqlParameter("@description", description));
                cmd.Parameters.Add(new SqlParameter("@keyWords", keyWords));
                cmd.ExecuteNonQuery();
                /*cmd = new SqlCommand(statementLastInserted, sqlConnection);
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                if (sqlDataReader.Read())
                {
                    int abstractId = sqlDataReader.GetInt32(0);
                    sqlConnection.Close();
                    sqlConnection.Open();
                    SqlCommand cmd2 = new SqlCommand(statementInsertContribution, sqlConnection);
                    cmd2.Parameters.Add(new SqlParameter("@authorId", authorId));
                    cmd2.Parameters.Add(new SqlParameter("@abstractId", abstractId));
                    cmd2.ExecuteNonQuery();
                }*/
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
        }

        public void UpdateAbstract(long abstractId, string abstractName, string description, string keyWords)
        {
            string statement = "Update Abstract Set AbstractName = @abstractName, Description = @description, KeyWords = @keyWords where AbstractId = @abstractId";

            string conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(statement, sqlConnection);
                cmd.Parameters.Add(new SqlParameter("@abstractId", abstractId));
                cmd.Parameters.Add(new SqlParameter("@abstractName", abstractName));
                cmd.Parameters.Add(new SqlParameter("@description", description));
                cmd.Parameters.Add(new SqlParameter("@keyWords", keyWords));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
        }

        public List<Author> GetAllAuthors()
        {
            String statement = "SELECT * FROM Author";
            List<Author> authors = new List<Author>();

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);

            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(statement, sqlConnection);

            try
            {
                SqlDataReader sqlDataReader = cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Author author = new Author();
                    author.authorId = sqlDataReader.GetInt32(0);
                    author.firstName = sqlDataReader.GetString(1);
                    author.lastName = sqlDataReader.GetString(2);
                    author.email = sqlDataReader.GetString(3);
                    author.affiliation = sqlDataReader.GetString(4);
                    author.password = sqlDataReader.GetString(5);
                    authors.Add(author);
                }
                Console.WriteLine(authors);
            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
            return authors;
        }

        public void AddProposal(int abstractId, string metaInfo)
        {
            string statement = "Insert into Proposal(AbstactId, MetaInfo) Values(@abstractId, @metaInfo)";

            string conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(statement, sqlConnection);
                cmd.Parameters.Add(new SqlParameter("@abstractId", abstractId));
                cmd.Parameters.Add(new SqlParameter("@metaInfo", metaInfo));
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(statement + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(statement + " - Connection closed");
            }
        }
        
        public void BuyTicket(long sessionId, long listenerId)
        {
            string sqlQuery = "INSERT INTO Ticket(SessionId,ListenerId) VALUES(@SID,@LID)";

            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, sqlConnection);
                cmd.Parameters.Add(new SqlParameter("@SID", sessionId));
                cmd.Parameters.Add(new SqlParameter("@LID", listenerId));
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(sqlQuery + " - Exception");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sqlConnection.Close();
                Console.WriteLine(sqlQuery + " - Connection closed");
            }
        }

    }
}