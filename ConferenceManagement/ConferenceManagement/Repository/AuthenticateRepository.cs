﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using ConferenceManagement.Models;

namespace ConferenceManagement.Repository
{
    public class AuthenticateRepository
    {
        /*
        public bool hasAccount(string username, string password)
        {
            String myConnectionString = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection conn;
            try
            {
                conn = new SqlConnection(myConnectionString);
                conn.Open();

                string commandText = "select * from UserLogIn where Username = '" + username + "'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataReader myreader = cmd.ExecuteReader();
                string pass;

                while (myreader.Read())
                {
                    pass = myreader.GetString(2);
                    if (pass != password)
                    {
                        myreader.Close();
                        return false;
                    }
                }
                myreader.Close();
            }
            catch (SqlException ex)
            {
                Console.Write(ex.Message);
            }

            return true;
        }*/
        
        public bool isAuthor(string email)
        {
            string statement = "SELECT COUNT(*) FROM Author WHERE Email = @email";

            int count = -1;
            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            SqlCommand verification = new SqlCommand(statement, sqlConnection);
            verification.Parameters.Add(new SqlParameter("@email", email));

            SqlDataReader sqlDataReader = verification.ExecuteReader();
            while (sqlDataReader.Read())
            {
                count = sqlDataReader.GetInt32(0);
            }

            if (count == 0)
                return false;
            return true;
        }

        public bool isPCMember(string email)
        {
            string statement = "SELECT Count(*) FROM PCMember WHERE Email = @email";

            int count = -1;
            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            SqlCommand verification = new SqlCommand(statement, sqlConnection);
            verification.Parameters.Add(new SqlParameter("@email", email));

            SqlDataReader sqlDataReader = verification.ExecuteReader();
            while (sqlDataReader.Read())
            {
                count = sqlDataReader.GetInt32(0);
            }

            if (count == 0)
                return false;
            return true;
        }

        public bool isListener(string email)
        {
            string statement = "SELECT COUNT(*) FROM [Listener] WHERE Email = @email";

            int count = -1;
            String conn = "Data Source = .\\SQLEXPRESS;" + "Initial Catalog = ConferenceManagement;" + "Integrated Security = true";
            SqlConnection sqlConnection = new SqlConnection(conn);
            sqlConnection.Open();
            SqlCommand verification = new SqlCommand(statement, sqlConnection);
            verification.Parameters.Add(new SqlParameter("@email", email));

            SqlDataReader sqlDataReader = verification.ExecuteReader();
            while (sqlDataReader.Read())
            {
                count = sqlDataReader.GetInt32(0);
            }

            if (count == 0)
                return false;
            return true;
        }
    }
}