﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConferenceManagement.Repository;
using ConferenceManagement.Models;
using ConferenceManagement.RuntimeObjects;

namespace ConferenceManagement.Controllers
{
    
    public class AssignController : Controller
    {
        ProposalDBControl dbControlProposal = new ProposalDBControl();

        // GET: Assign
        public ActionResult Index()
        {
            List<Evaluation> evaluations = dbControlProposal.GetAllEvaluations();
            return View(evaluations);
        }

        // GET: Assign/Papers
        public ActionResult Papers()
        {
            //Only if the deadline for bidding is over
            List<Paper> papers = DbInstance.GetDbInstance().GetAllBidAcceptedPapers();
            return View(papers);
        }

        // GET: Assign/Paper/{id}
        public ActionResult Paper(int id)
        {
            Paper paper = DbInstance.GetDbInstance().getPaper(id);
            return View(paper);
        }

        public ActionResult Assign()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Assign(Evaluation evaluation)
        {
            dbControlProposal.AssignPaper(evaluation.PaperId, evaluation.ReviewerId);
            return RedirectToAction("Papers");
        }
        
    }
}