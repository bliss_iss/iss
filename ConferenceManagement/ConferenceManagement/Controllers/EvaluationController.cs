﻿using ConferenceManagement.Models;
using ConferenceManagement.Repository;
using ConferenceManagement.RuntimeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConferenceManagement.Controllers
{
    public class EvaluationController : Controller
    {
        DbInstance dbInstance = DbInstance.GetDbInstance();
        MockInstance mockInstance = MockInstance.GetDbInstance();

        // GET: Evaluation
        public ActionResult Index()
        {
            return View();
        }

        // GET: Evaluation/Paper/{id}
        public ActionResult Paper(int id)
        {
            // GET CURRENT REVIEWER
            Reviewer reviewer = mockInstance.getCurrentReviewer();
            Evaluation evaluation = dbInstance.getEvaluation(reviewer.PCId, id);

            ViewBag.Paper = evaluation.Paper;
            
            return View(evaluation);
        }

        // GET: Evaluation/Paper_all/{id}
        public ActionResult Paper_all(long id)
        {
            List<Evaluation> evaluations = dbInstance.getPaperEvaluations(id);
            Paper paper = dbInstance.getPaper(id);
            ViewBag.Paper = paper;
            return View(evaluations);
        }
    }
}