﻿using ConferenceManagement.Models;
using ConferenceManagement.Repository;
using ConferenceManagement.RuntimeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConferenceManagement.Controllers
{
    public class AuthorLoginController : Controller
    {
        DbInstance dbControls = DbInstance.GetDbInstance();
        MockInstance mockRepo = MockInstance.GetDbInstance();
        AuthenticateRepository repo = new AuthenticateRepository();

        // GET: AuthorLogin
        public ActionResult Index()
        {
            Author author = mockRepo.getCurrentAuthor();
            if (author == null)
                return RedirectToAction("Login");
            return View(author);
        }

        // GET: AuthorLogin/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: AuthorLogin/Login
        [HttpPost]
        public ActionResult Login(Author author)
        {
            string email = author.email;
            string password = author.password;
            if (repo.isAuthor(email))
            {
                MockInstance.GetDbInstance().LoginAuthor(email, password);
                return RedirectToAction("Index", "Author");
            }
            if (repo.isPCMember(email))
            {

                mockRepo.LoginPC(email, password);
                return RedirectToAction("Index", "Cchair");
            }
            if (repo.isListener(email))
            {
                mockRepo.LoginListener(email, password);
                return RedirectToAction("Index", "Listener");
            }
            return View();
        }

        // GET: AuthorLogin/Register
        public ActionResult Register()
        {
            return View();
        }

        // POST: AuthorLogin/Register
        [HttpPost]
        public ActionResult Register(Author author)
        {
            
           DbInstance.GetDbInstance().RegisterAuthor(author);
           mockRepo.LoginAuthor(author.email, author.password);
           return RedirectToAction("Index", "Author");
            
        }
    }
}