﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConferenceManagement.Repository;
using ConferenceManagement.Models;
using ConferenceManagement.RuntimeObjects;

namespace HolidayDestinations.Controllers
{

    public class BidController : Controller
    {
        ProposalDBControl dBControl = new ProposalDBControl();
        
        // GET: Bid
        public ActionResult Index()
        {
            List<Bid> bids = dBControl.GetAllBids();
            return View(bids);
        }

        // GET: Bid/Proposals
        public ActionResult Proposals()
        {
            List<Proposal> proposals = dBControl.GetAllProposals();
            return View(proposals);
        }

        // GET: Bid/Abstract/{id}
        public ActionResult Abstract(int id)
        {
            Abstract abstractPaper = dBControl.GetAbstractById(id);
            return View(abstractPaper);
        }

        // POST: Bid/Abstract/{id}
        [HttpPost]
        public ActionResult Abstract(Abstract abstractPaper)
        {
            long pcId = MockInstance.GetDbInstance().getCurrentProgramCommitee().PCId;
            long abstractId = abstractPaper.Id;
            string status = Request["Bid"].ToString();   //string status2 = Request.Form["Bid"].ToString();
            dBControl.BidProposal(pcId, abstractId, status);
            return RedirectToAction("Proposals");
        }

        // GET: Bid/Abstracts
        public ActionResult Abstracts()
        {
            List<Abstract> abstracts = dBControl.GetAllAbstracts();
            return View(abstracts);
        }
    }
}