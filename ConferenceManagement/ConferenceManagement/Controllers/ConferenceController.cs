﻿using ConferenceManagement.Models;
using ConferenceManagement.Repository;
using ConferenceManagement.RuntimeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ConferenceManagement.Controllers
{
    public class ConferenceController : Controller
    {
        DbInstance dbControls= DbInstance.GetDbInstance();
        MockInstance mockRepo = MockInstance.GetDbInstance();

        // GET: Conference
        public ActionResult Index()
        {
            IEnumerable<Conference> conferences = dbControls.getAllConferences();
            Conference conference = mockRepo.getCurrentConference();

            if (conferences.Count() != 0)
            {
                ViewBag.CurrentConference = conference;
                return View(conferences);
            }
            else
            {
                return RedirectToAction("Create");
            }
        }

        // GET: Conference/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Conference/Create
        [HttpPost]
        public ActionResult Create(Conference conference)
        {
            try
            {
                dbControls.AddConference(conference);
                return RedirectToAction("Index");
            }
            catch
            {
                // REDIRECT LA ERROR PAGE
                return RedirectToAction("Index");
            }
        }

        // GET: Conference/ChangeCurrentConference/{id}
        public ActionResult ChangeCurrentConference(long id)
        {
            mockRepo.SetCurrentConference(id);
            return RedirectToAction("Index");
        }

        // GET: Conference/AuthorDeadlines
        public ActionResult AuthorDeadlines()
        {
            return View();
        }

        // POST: Conference/Create
        [HttpPost]
        public ActionResult AuthorDeadlines(AuthorDeadlines AuthorDeadlines)
        {
            try
            {
                AuthorDeadlines.ConfId = MockInstance.GetDbInstance().getCurrentConference().ConfId;
                dbControls.AddAuthorDeadlines(AuthorDeadlines);
                return RedirectToAction("Index");
            }
            catch
            {
                // REDIRECT LA ERROR PAGE
                return RedirectToAction("Index");
            }
        }

        // GET: Conference/EvaluationDeadlines
        public ActionResult EvaluationDeadlines()
        {
            return View();
        }

        // POST: Conference/Create
        [HttpPost]
        public ActionResult EvaluationDeadlines(EvaluationDeadlines EvaluationDeadlines)
        {
            try
            {
                EvaluationDeadlines.ConfId = MockInstance.GetDbInstance().getCurrentConference().ConfId;
                dbControls.AddEvaluationDeadlines(EvaluationDeadlines);
                return RedirectToAction("Index");
            }
            catch
            {
                // REDIRECT LA ERROR PAGE
                return RedirectToAction("Index");
            }
        }

        // GET: Conference/Deadlines
        public ActionResult Deadlines()
        {
            Conference currentConference = MockInstance.GetDbInstance().getCurrentConference();

            AuthorDeadlines authorDeadlines = DbInstance.GetDbInstance().GetAuthorDeadlinesByConf(currentConference.ConfId);
            ViewBag.AuthorDeadlines = authorDeadlines;

            EvaluationDeadlines evaluationDeadlines = DbInstance.GetDbInstance().GetEvaluationDeadlinesByConf(currentConference.ConfId);
            ViewBag.EvaluationDeadlines = evaluationDeadlines;

            return View();
        }

    }
}