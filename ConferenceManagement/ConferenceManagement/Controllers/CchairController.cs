﻿using ConferenceManagement.Models;
using ConferenceManagement.RuntimeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HolidayDestinations.Controllers
{
    public class CchairController : Controller
    {
        // GET: Cchair
        public ActionResult Index()
        {
            ProgramCommittee programCommittee = MockInstance.GetDbInstance().getCurrentProgramCommitee();
            return View(programCommittee);
        }

        // GET: Cchair/Deadlines
        public ActionResult Deadlines()
        {
            // aici am putea redirecta la ceva gen Conference/Edit
            return View();
        }

        // GET: Cchair/Sessions
        public ActionResult Sessions()
        {
            return View();
        }
    }
}