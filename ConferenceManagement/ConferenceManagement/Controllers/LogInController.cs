﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConferenceManagement.Repository;
using ConferenceManagement.Models;

namespace ConferenceManagement.Controllers
{
    public class LogInController : Controller
    {
        DbControls dbControls = new DbControls();
        MockRepo mockRepo = new MockRepo();
        AuthenticateRepository repo = new AuthenticateRepository();

        // GET: PCLogIn
        public ActionResult Index()
        {
            ProgramCommittee programCommittee = mockRepo.getCurrentProgramCommitee();
            if (programCommittee.PCId == 0 || programCommittee.PCId == -1)
                return RedirectToAction("Login");
            return View(programCommittee);
        }

        // GET: PCLogIn/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: PCLogIn/Login
        [HttpPost]
        public ActionResult Login(ProgramCommittee pc)
        {
            string email = pc.Email;
            string password = pc.Password;
            if (repo.isAuthor(email))
                return RedirectToAction("Index", "Author");
            if (repo.isPCMember(email))
                return RedirectToAction("Index", "Reviewer");
            if (repo.isListener(email))
                return RedirectToAction("Index", "Listener");
            return View();
        }
    }
}