﻿using ConferenceManagement.Models;
using ConferenceManagement.Repository;
using ConferenceManagement.RuntimeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConferenceManagement.Controllers
{
    public class SessionController : Controller
    {
        DbInstance dbControls = DbInstance.GetDbInstance();
        ProposalDBControl proposalDbControl = new ProposalDBControl();

        // GET: Session
        public ActionResult Index()
        {
            List<Session> sessions = proposalDbControl.GetAllSessions();
            return View(sessions);
        }

        // GET: Session/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Session/Create
        [HttpPost]
        public ActionResult Create(Session session)
        {
            try
            {
                dbControls.AddSession(session);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}