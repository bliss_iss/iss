﻿using ConferenceManagement.Models;
using ConferenceManagement.Repository;
using ConferenceManagement.RuntimeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConferenceManagement.Controllers
{
    public class ListenerLoginController : Controller
    {
        DbInstance dbControls = DbInstance.GetDbInstance();
        MockInstance mockRepo = MockInstance.GetDbInstance();
        AuthenticateRepository repo = new AuthenticateRepository();

        // GET: ListenerLogin
        public ActionResult Index()
        {
            Listener listener = mockRepo.getCurrentListener();
            if (listener == null)
                return RedirectToAction("Login");
            return View(listener);
        }

        // GET: ListenerLogin/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: ListenerLogin/Login
        [HttpPost]
        public ActionResult Login(Listener listener)
        {
            string email = listener.Email;
            string password = listener.Password;
            if (repo.isAuthor(email))
            {
                MockInstance.GetDbInstance().LoginAuthor(email, password);
                return RedirectToAction("Index", "Author");
            }
            if (repo.isPCMember(email))
            {

                mockRepo.LoginPC(email, password);
                return RedirectToAction("Index", "Cchair");
            }
            if (repo.isListener(email))
            {
                mockRepo.LoginListener(email, password);
                return RedirectToAction("Index", "Listener");
            }
            return View();
        }

        // GET: ListenerLogin/Register
        public ActionResult Register()
        {
            return View();
        }

        // POST: ListenerLogin/Register
        [HttpPost]
        public ActionResult Register(Listener listener)
        {

            DbInstance.GetDbInstance().RegisterListener(listener);
            mockRepo.LoginListener(listener.Email, listener.Password);
            return RedirectToAction("Index", "Listener");

        }
    }
}