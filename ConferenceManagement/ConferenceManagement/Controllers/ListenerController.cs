﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConferenceManagement.Repository;
using ConferenceManagement.Models;
using ConferenceManagement.RuntimeObjects;

namespace ConferenceManagement.Controllers
{
    public class ListenerController : Controller
    {
        ProposalDBControl dBControlProposal = new ProposalDBControl();
        DbInstance dbControls = DbInstance.GetDbInstance();
        MockInstance mockRepo = MockInstance.GetDbInstance();

        // GET: Listener
        public ActionResult Index()
        {
            return View();
        }

        // GET: Listener/Sessions
        public ActionResult Sessions()
        {
            List<Session> sessions = dBControlProposal.GetAllSessions();
            return View(sessions);
        }

        // GET: Listener/OneSession/{id}
        public ActionResult OneSession(long id)
        {
            Session session = dBControlProposal.GetSession(id);
            return View(session);
        }

        //GET: Listener/Session
        public ActionResult BuyTicket(long id)
        {
            long sessionId = id;
            long currentListenerId = mockRepo.getCurrentListener().ListenerId;
            dBControlProposal.BuyTicket(sessionId, currentListenerId);

            return RedirectToAction("Sessions");
        }
    }
}