﻿using ConferenceManagement.Models;
using ConferenceManagement.Repository;
using ConferenceManagement.RuntimeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ConferenceManagement.Controllers
{
    public class AuthorController : Controller
    {
        ProposalDBControl dbControl = new ProposalDBControl();
        
        // GET: Author
        public ActionResult Index()
        {
            Author author = MockInstance.GetDbInstance().getCurrentAuthor();
            if (author == null)
            {
                return RedirectToAction("Index", "Home", null);
            }
            //Author author = new Author(1, "Vladutu", "Greutu", "xXVladutuTau69Xx@HOTmail.com", "Femei", "nuvazic");
            List<Abstract> abstracts = DbInstance.GetDbInstance().GetAuthorAbstracts(author.authorId);
            /*
            List<Abstract> allAbstracts = dbControl.GetAllAbstracts();
            List<Abstract> authorAbstracts = new List<Abstract>();
            foreach(Abstract abs in allAbstracts)
            {
                foreach(Author a in abs.Authors)
                {
                    if (a.authorId == author.authorId)
                        authorAbstracts.Add(abs);
                }
            }
            IEnumerable<Abstract> finalAbstracts = authorAbstracts;
            */
            return View(abstracts);
        }

        public ActionResult Submit(Abstract abs)
        {
            Author author = MockInstance.GetDbInstance().getCurrentAuthor();
            if (author == null)
            {
                return RedirectToAction("Index", "Home", null);
            }
            return View("Index");
        }

        public ActionResult Show(string searchString)
        {
            IEnumerable<Author> authors = dbControl.GetAllAuthors();
            if (!String.IsNullOrEmpty(searchString))
            {
                authors = authors.Where(a => a.email.Contains(searchString));

            }
            return View(authors);

        }

        public ActionResult Edit(Abstract abs)
        {
            Author author = MockInstance.GetDbInstance().getCurrentAuthor();
            if (author == null)
            {
                return RedirectToAction("Index", "Home", null);
            }
            return RedirectToAction("Edit", "Abstract", new { area = "" });
        }

        [HttpGet]
        public ActionResult Submit(int id)
        {
            Author author = MockInstance.GetDbInstance().getCurrentAuthor();
            if (author == null)
            {
                return RedirectToAction("Index", "Home", null);
            }
            Abstract absPaper = dbControl.GetAbstractById(id);
            string metaInfo = "Meta-Info about " + absPaper.Name;
            dbControl.AddProposal(id, metaInfo);
            return View(absPaper);
        }

        public ActionResult AddContributors(int abstractId, int authorId)
        {
            return null;
        }
    }
}