﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConferenceManagement.Repository;
using ConferenceManagement.Models;
using ConferenceManagement.RuntimeObjects;

namespace ConferenceManagement.Controllers
{
    public class PCLogInController : Controller
    {
        DbInstance dbControls = DbInstance.GetDbInstance();
        MockInstance mockRepo = MockInstance.GetDbInstance();
        AuthenticateRepository repo = new AuthenticateRepository();

        // GET: PCLogIn
        public ActionResult Index()
        {
            ProgramCommittee programCommittee = mockRepo.getCurrentProgramCommitee();
            if (programCommittee == null)
                return RedirectToAction("Login");
            return View(programCommittee);
        }

        // GET: PCLogIn/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: PCLogIn/Login
        [HttpPost]
        public ActionResult Login(ProgramCommittee pc)
        {
            string email = pc.Email;
            string password = pc.Password;
            if (repo.isAuthor(email))
            {
                MockInstance.GetDbInstance().LoginAuthor(email, password);
                return RedirectToAction("Index", "Author");
            }
            if (repo.isPCMember(email))
            {

                mockRepo.LoginPC(email, password);
                return RedirectToAction("Index", "Cchair");
            }
            if (repo.isListener(email))
            {
                mockRepo.LoginListener(email, password);
                return RedirectToAction("Index", "Listener");
            }
            return View();
        }

        // GET: PCLogIn/Login
        public ActionResult Register()
        {
            return View();
        }

        // POST: PCLogIn/Login
        [HttpPost]
        public ActionResult Register(ProgramCommittee pc)
        {
            string a = MockInstance.GetDbInstance().getCurrentConference().SecretKey;
            if (pc.SecretKey == MockInstance.GetDbInstance().getCurrentConference().SecretKey)
            {
                DbInstance.GetDbInstance().RegisterPC(pc);
                mockRepo.LoginPC(pc.Email, pc.Password);
                return RedirectToAction("Index", "Cchair");
            }
            return View("Index", "Home");
        }

        public ActionResult Logout()
        {
            mockRepo.ResetLogin();
            return RedirectToAction("Index", "Home");
        }
    }
}