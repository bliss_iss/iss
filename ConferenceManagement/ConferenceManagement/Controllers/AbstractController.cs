﻿using ConferenceManagement.Models;
using ConferenceManagement.Repository;
using ConferenceManagement.RuntimeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConferenceManagement.Controllers
{
    
    public class AbstractController : Controller
    {
        ProposalDBControl dbControl = new ProposalDBControl();

        // GET: Abstract
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            
            return View();
        }

        public ActionResult Edit(Abstract abs)
        {
            try
            {
                dbControl.UpdateAbstract(abs.Id, abs.Name, abs.Description, abs.Keywords);
            }
            catch
            {
                return View();
            }
            return RedirectToAction("Index", "Author");
        }

        [HttpPost]
        public ActionResult Create(Abstract absPaper)
        { 
            try
            {
                string name = absPaper.Name;
                string description = absPaper.Description;
                string keyWords = absPaper.Keywords;

                Author author = MockInstance.GetDbInstance().getCurrentAuthor();

                dbControl.CreateAbstract(author.authorId, name, description, keyWords);
                Abstract Abstract = dbControl.GetAllAbstracts().Find(a => a.Name.Equals(name) && a.Keywords.Equals(keyWords));
                dbControl.CreateContribution(author.authorId, Abstract.Id);
                return RedirectToAction("Index","Author", new { area = "" });
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Edit(int id, string search)
        {
            Abstract abs = dbControl.GetAbstractById(id);
            ViewBag.Authors = dbControl.GetAllAuthors();
            IEnumerable <Author> authors = dbControl.GetAllAuthors();
            if (!String.IsNullOrEmpty(search))
            {
                authors = authors.Where(a => a.email.Contains(search));
            }
            return View(abs);


        }


    }
}