﻿using ConferenceManagement.Models;
using ConferenceManagement.Repository;
using ConferenceManagement.RuntimeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HolidayDestinations.Controllers
{
    public class ReviewerController : Controller
    {
        DbInstance dbControls = DbInstance.GetDbInstance();
        MockInstance mockRepo = MockInstance.GetDbInstance();

        // GET: Reviewer
        public ActionResult Index()
        {            
            Reviewer reviewer = mockRepo.getCurrentReviewer();
            if (reviewer == null)
                return RedirectToAction("Index", "Home");
            return View(reviewer);
        }

        // GET: Reviewer/Review
        public ViewResult Review(string filterString)
        {
            // GET CURRENT REVIEWER
            Reviewer reviewer = mockRepo.getCurrentReviewer();
            IEnumerable<Paper> papers = dbControls.getAssignedPapers(reviewer.PCId);


            if (!String.IsNullOrEmpty(filterString))
            {
                ViewBag.Options = "See evaluation";
                // Show papers pending review
                papers = papers.Where(p =>
                {
                    Evaluation evaluation = dbControls.getEvaluation(reviewer.PCId, p.Id);
                    if (evaluation != null && evaluation.Mark.value != "NOT PROCESSED")
                    {
                        return true;
                    }
                    return false;
                });
            }
            else
            {
                ViewBag.Options = "Review";
                // Show papers reviewed
                papers = papers.Where(p =>
                {
                    Evaluation evaluation = dbControls.getEvaluation(reviewer.PCId, p.Id);
                    if (evaluation != null && evaluation.Mark.value == "NOT PROCESSED")
                    {
                        return true;
                    }
                    return false;
                });
            }
            return View(papers);
        }

        // GET: Reviewer/Paper/{id}
        public ActionResult Paper(int id)
        {
            Paper paper = dbControls.getPaper(id);
            Reviewer reviewer = mockRepo.getCurrentReviewer();
            Evaluation evaluation = dbControls.getEvaluation(reviewer.PCId, paper.Id);
            if(evaluation.Mark.value == "NOT PROCESSED") 
                return View(paper);
            else {
                ViewBag.Paper = evaluation.Paper;
                return View("~/Views/Evaluation/Paper.cshtml", evaluation);
            }
        }

        // POST: Reviewer/Paper/{id}
        [HttpPost]
        public ActionResult Paper(Paper paper)
        {
            long reviewerId = mockRepo.getCurrentReviewer().PCId;
            long paperId = paper.Id;
            string mark = Request["Mark"];
            string recommendation = Request["Recommendation"];
            this.dbControls.updatePaper(reviewerId, paperId, mark, recommendation);
            return RedirectToAction("Review");
        }
    }
}