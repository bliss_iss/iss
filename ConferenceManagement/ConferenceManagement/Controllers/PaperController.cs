﻿using ConferenceManagement.Models;
using ConferenceManagement.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ConferenceManagement.Controllers
{
    public class PaperController : Controller
    {
        DbControls dbControls = new DbControls();
        ProposalDBControl proposalDb = new ProposalDBControl(); 
        MockRepo mockRepo = new MockRepo();
        // GET: Paper
        public ActionResult Index()
        {
            return View();
        }

        // GET: Paper/Create/{id}
        public ActionResult Create(int id)
        {
            Paper paper = new Paper();
            Abstract Abstract = proposalDb.GetAbstractById(id);
            paper.AbstractId = Abstract.Id;
            return View(paper);
        }

        // POST: Paper/Create/
        [HttpPost]
        public ActionResult Create(Paper paper)
        {
            try
            {
                string title = paper.Title;
                string url = paper.PaperUrl;
                dbControls.CreatePaper(title, url, paper.AbstractId);
                return RedirectToAction("Index", "Author", null);
            }
            catch
            {
                return View();
            }
        }

        /*
        // POST: Paper/Create/{abstractId}
        [HttpPost]
        public ActionResult Create(Paper paper, long abstractId)
        {
            try
            {
                string title = paper.Title;
                string url = paper.PaperUrl;
                Abstract abs = dbControls.getAbstract(abstractId);
                dbControls.CreatePaper(title, url, abs.Id);
                return RedirectToAction("Index", "Author", null);
            }
            catch
            {
                return View();
            }
        }*/

        // GET: Paper/Delete/{id}
        public ActionResult Delete(int? Id)
        {
            // check if valid id, return view of paper
            return View();
        }

        // POST: Paper/Delete/{id}
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int Id)
        {
            // Delete logic here
            return RedirectToAction("Index");
        }

        // GET: Paper/Details/{id}
        public ActionResult Details(int Id)
        {
            // check if valid id, return view of paper
            return View();
        }

        // GET: Paper/Edit/{id}
        public ActionResult Edit(int Id)
        {
            // check if valid id, return view of paper
            return View();
        }

        // POST: Paper/Edit/{id}
        [HttpPost]
        public ActionResult Edit(Paper paper)
        {
            try
            {
                // Edit logic here
                return RedirectToAction("Details", new { id = paper.Id });
            }
            catch
            {
                return View();
            }
        }
    }
}